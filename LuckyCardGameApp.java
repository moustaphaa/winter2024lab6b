import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Deck mydeck = new Deck();
		System.out.println("Welcome! Enter the number of cards you wish to remove:");
		System.out.println(mydeck.length());
		int remove = reader.nextInt();
		System.out.println(mydeck.length() - remove);
		mydeck.shuffle();
		System.out.println(mydeck);
	}
}